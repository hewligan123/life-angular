import { Component, OnInit, OnChanges, Input, Output } from '@angular/core';
import { gameStateDescriptionList, GameStateDescription } from '../lib/gameTypes';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pattern-selector',
  templateUrl: './pattern-selector.component.html',
  styleUrls: ['./pattern-selector.component.scss']
})
export class PatternSelectorComponent implements OnInit, OnChanges {

  @Input() stateList: gameStateDescriptionList;

  @Output() selectedStateChange = new EventEmitter<GameStateDescription>();

  selectedState: GameStateDescription;

  constructor() { }

  ngOnInit() {}

  ngOnChanges() {
    if (!this.selectedState && this.stateList.length) {
      this.selectedState = this.stateList[0];
    }
  }

  update(e) {
    this.selectedStateChange.emit(this.selectedState);
  }

}

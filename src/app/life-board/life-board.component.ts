import { Component, OnInit, Input } from '@angular/core';

import { gameState } from '../lib/gameTypes';

@Component({
  selector: 'app-life-board',
  templateUrl: './life-board.component.html',
  styleUrls: ['./life-board.component.scss']
})
export class LifeBoardComponent implements OnInit {

  @Input() gameState: gameState;

  constructor() { }

  ngOnInit() {
  }

}

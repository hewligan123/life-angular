import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeRowComponent } from '../life-row/life-row.component';
import { LifeCellComponent } from '../life-cell/life-cell.component';
import { LifeBoardComponent } from './life-board.component';

describe('LifeBoardComponent', () => {
  let component: LifeBoardComponent;
  let fixture: ComponentFixture<LifeBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LifeRowComponent,
        LifeCellComponent,
        LifeBoardComponent,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

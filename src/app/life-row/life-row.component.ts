import { Component, OnInit, Input } from '@angular/core';

import { gameRowState } from '../lib/gameTypes';

@Component({
  selector: 'app-life-row',
  templateUrl: './life-row.component.html',
  styleUrls: ['./life-row.component.scss']
})
export class LifeRowComponent implements OnInit {

  @Input() state: gameRowState;

  constructor() { }

  ngOnInit() {}

}

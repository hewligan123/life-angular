import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeCellComponent } from '../life-cell/life-cell.component';
import { LifeRowComponent } from './life-row.component';

describe('LifeRowComponent', () => {
  let component: LifeRowComponent;
  let fixture: ComponentFixture<LifeRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LifeCellComponent,
        LifeRowComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

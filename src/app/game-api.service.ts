import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { gameStateDescriptionList, GameStateDescription } from './lib/gameTypes';

const API_ROOT = 'http://localhost:3000/game-starts/';

@Injectable({
  providedIn: 'root'
})
export class GameApiService {

  constructor(private http: HttpClient) {}

  getGameSummaries() {
    return this.http.get<gameStateDescriptionList>(API_ROOT);
  }

  getGameDetails(summary: GameStateDescription) {
    return this.http.get<GameStateDescription>(`${API_ROOT}${summary._id}`);
  }
}

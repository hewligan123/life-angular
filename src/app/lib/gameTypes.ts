
export type gameCellState = boolean;
export type gameRowState = gameCellState[];
export type gameState = gameRowState[];

export interface GameStateDescription {
    name: string;
    _id: string;
    width: number;
    height: number;
    initialState: gameState;
}

export interface GameStateDescriptionMap {
    [x: string]: GameStateDescription;
}

export type gameStateDescriptionList = GameStateDescription[];

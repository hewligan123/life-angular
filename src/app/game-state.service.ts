import { Injectable } from '@angular/core';

import { gameState, gameRowState, gameCellState } from './lib/gameTypes';
import { timeout } from 'q';
import { Observable, Observer } from 'rxjs';

const getNextPosition = (length: number, position: number) => position > (length - 2) ? 0 : (position + 1);
const getPreviousPosition = (length: number, position: number) => position < 1 ? length - 1 : (position - 1);

const getCellState = (boardState: gameState, row: number, col: number) => {
    const isAlive = boardState[row][col];
    const prevRowIndex = getPreviousPosition(boardState.length, row);
    const nextRowIndex = getNextPosition(boardState.length, row);
    const prevColIndex = getPreviousPosition(boardState[0].length, col);
    const nextColIndex = getNextPosition(boardState[0].length, col);

    const prevRow = boardState[prevRowIndex];
    const currentRow = boardState[row];
    const nextRow = boardState[nextRowIndex];

    const surroundingScore = (
        Number(prevRow[prevColIndex]) + Number(prevRow[col]) + Number(prevRow[nextColIndex])
        + Number(currentRow[prevColIndex]) + Number(currentRow[nextColIndex])
        + Number(nextRow[prevColIndex]) + Number(nextRow[col]) + Number(nextRow[nextColIndex])
    );

    if (isAlive && surroundingScore > 1 && surroundingScore < 4) {
        return A;
    }

    if (!isAlive && surroundingScore === 3) {
        return A;
    }

    return D;
};

const A = true;
const D = false;
const DEFAULT_TIMEOUT = 500;

@Injectable({
  providedIn: 'root'
})
export class GameStateService {

  private gameState: gameState;
  private timeout: number;
  private interval: number;
  private observer: Observer<gameState>;
  private observable: Observable<gameState>;

  constructor() {
    this.gameState = [];
    this.timeout = DEFAULT_TIMEOUT;
    this.observable = new Observable(observer => {
      this.observer = observer;
      this.observer.next(this.gameState);
    });
  }

  private nextState() {
    this.gameState = this.gameState.map(
      (row: gameRowState, rowNumber: number) => row.map(
          (cell: gameCellState, cellNumber: number) => getCellState(this.gameState, rowNumber, cellNumber)
      )
    );

    this.observer.next(this.gameState);

    return this;
  }

  setState(newState: gameState) {
    this.gameState = newState;
    if (this.observer) {
      this.observer.next(this.gameState);
    }

    return this;
  }

  setStepTime(newTimeout: number) {
    this.timeout = newTimeout;
    if (this.interval) {
      this.stop();
      this.start();
    }
    return this;
  }

  getStepTime() {
    return timeout;
  }

  step() {
    return this.nextState();
  }

  start() {
    if (!this.interval) {
      this.interval = window.setInterval(this.step.bind(this), this.timeout);
    }

    return this;
  }

  stop() {
    if (this.interval) {
      window.clearInterval(this.interval);
      this.interval = null;
    }

    return this;
  }

  isRunning() {
    return Boolean(this.interval);
  }

  subscribe({next, error, complete}) {
    this.observable.subscribe({next, error, complete});

    return this;
  }

}

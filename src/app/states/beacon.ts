const A = true;
const D = false;

export const beaconInitialState = [
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, A, A, D, D, D],
    [D, D, D, D, D, A, A, D, D, D],
    [D, D, D, A, A, D, D, D, D, D],
    [D, D, D, A, A, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
];

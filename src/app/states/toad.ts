const A = true;
const D = false;

export const toadInitialState = [
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, A, A, A, D, D, D, D],
    [D, D, D, D, A, A, A, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
];
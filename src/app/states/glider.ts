const A = true;
const D = false;

export const gliderInitialState = [
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, A, D, D, D, D, D, D, D],
    [D, D, D, A, A, D, D, D, D, D],
    [D, D, A, A, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
];
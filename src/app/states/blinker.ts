const A = true;
const D = false;

export const blinkerInitialState = [
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, A, D, D, D, D, D],
    [D, D, D, D, A, D, D, D, D, D],
    [D, D, D, D, A, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
    [D, D, D, D, D, D, D, D, D, D],
];
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

import { GameApiService } from './game-api.service';

describe('GameApiService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
    });
  });

  it('should be created', () => {
    const service: GameApiService = TestBed.get(GameApiService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

// import { beaconInitialState } from './states/beacon';
import { GameStateService } from './game-state.service';
import { GameApiService } from './game-api.service';
import { gameStateDescriptionList, GameStateDescription, gameState } from './lib/gameTypes';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Life';
  ctrlButtonText = 'Start';
  gameState: gameState = [];
  gameSummaries: gameStateDescriptionList = [];

  constructor(
    private gameStateService: GameStateService,
    private gameApi: GameApiService,
  ) {}

  ngOnInit() {
    this.gameStateService.setState(this.gameState);
    this.gameStateService.subscribe({
      next: newState => this.gameState = newState,
      error: e => console.error(e),
      complete: () => {}
    });

    this.gameApi.getGameSummaries()
      .subscribe({
        next: result => this.setInitialGame(result),
        error: e => console.error(e),
        complete: () => {}
      });
  }

  setInitialGame(list: gameStateDescriptionList) {
    if (list.length) {
      this.loadPattern(list[0]);
      this.gameSummaries = list;
    }
  }

  toggleStart() {
    if (this.ctrlButtonText === 'Start') {
      this.gameStateService.start();
      this.ctrlButtonText = 'Stop';
    } else {
      this.gameStateService.stop();
      this.ctrlButtonText = 'Start';
    }
  }

  step() {
    this.gameStateService.step();
  }

  setPattern(pattern: GameStateDescription) {
    this.gameStateService.setState(pattern.initialState);
  }

  loadPattern(pattern: GameStateDescription) {
    this.gameApi.getGameDetails(pattern)
      .subscribe({
        next: newPattern => this.setPattern(newPattern)
      });
  }

}

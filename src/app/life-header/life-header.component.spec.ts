import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeHeaderComponent } from './life-header.component';

describe('LifeHeaderComponent', () => {
  let component: LifeHeaderComponent;
  let fixture: ComponentFixture<LifeHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

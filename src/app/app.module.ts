import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LifeHeaderComponent } from './life-header/life-header.component';
import { LifeBoardComponent } from './life-board/life-board.component';
import { LifeCellComponent } from './life-cell/life-cell.component';
import { LifeRowComponent } from './life-row/life-row.component';
import { LifeFooterComponent } from './life-footer/life-footer.component';
import { PatternSelectorComponent } from './pattern-selector/pattern-selector.component';

@NgModule({
  declarations: [
    AppComponent,
    LifeHeaderComponent,
    LifeBoardComponent,
    LifeCellComponent,
    LifeRowComponent,
    LifeFooterComponent,
    PatternSelectorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

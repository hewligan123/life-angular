import { Component, OnInit, Input } from '@angular/core';

import { gameCellState } from '../lib/gameTypes';

@Component({
  selector: 'app-life-cell',
  templateUrl: './life-cell.component.html',
  styleUrls: ['./life-cell.component.scss']
})
export class LifeCellComponent implements OnInit {

  @Input() state: gameCellState;

  constructor() { }

  ngOnInit() {
  }

}

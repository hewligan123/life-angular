import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeCellComponent } from './life-cell.component';

describe('LifeCellComponent', () => {
  let component: LifeCellComponent;
  let fixture: ComponentFixture<LifeCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

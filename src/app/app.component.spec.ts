import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { LifeHeaderComponent } from '../app/life-header/life-header.component';
import { LifeBoardComponent } from '../app/life-board/life-board.component';
import { LifeRowComponent } from '../app/life-row/life-row.component';
import { LifeCellComponent } from '../app/life-cell/life-cell.component';
import { LifeFooterComponent } from '../app/life-footer/life-footer.component';
import { PatternSelectorComponent } from '../app/pattern-selector/pattern-selector.component';

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        HttpClientTestingModule,
      ],
      declarations: [
        LifeHeaderComponent,
        LifeBoardComponent,
        LifeRowComponent,
        LifeCellComponent,
        LifeFooterComponent,
        PatternSelectorComponent,
        AppComponent
      ],
    }).compileComponents();

  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Life'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Life');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Life');
  });
});

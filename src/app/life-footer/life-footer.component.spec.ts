import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeFooterComponent } from './life-footer.component';

describe('LifeFooterComponent', () => {
  let component: LifeFooterComponent;
  let fixture: ComponentFixture<LifeFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
